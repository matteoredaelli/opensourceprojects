+++
title = "Libraries.io metrics"
description = "Programming languages metrics from Libraries.io.com: packages count"
categories = ['programming languages']
tags = ["libraries.io"]
date = "2020-09-22"
draft = false
+++

[Libraries.io](https://www.libraries.io.com/) "is an open source web service that lists software development project dependencies and alerts developers to new versions of the software libraries they are using." [Wikipedia](https://en.wikipedia.org/wiki/Libraries.io)

<!--more-->

## Libraries.io packages

{{< preformatted_text file="/static/metrics_libraries_io_project_count.txt" >}}


Last update:
{{< text file="/static/metrics_libraries_io_project_count.txt.timestamp" >}}
