+++
title = "Github metrics"
description = "Programming languages metrics from Github.com: active users and subscribers"
categories = ['programming languages']
tags = ["github"]
date = "2020-09-22"
draft = false
+++

[Github](https://www.github.com/) "is an American multinational corporation that provides hosting for software development and version control using Git." [Wikipedia](https://en.wikipedia.org/wiki/Github)

<!--more-->

## Github forks

{{< preformatted_text file="/static/metrics_github_forks.txt" >}}

## Github open issues

{{< preformatted_text file="/static/metrics_github_open_issues.txt" >}}

## Github stars

{{< preformatted_text file="/static/metrics_github_stars.txt" >}}

## Github watchers

{{< preformatted_text file="/static/metrics_github_watchers.txt" >}}

Last update:
{{< text file="/static/metrics_github_stars.txt.timestamp" >}}
