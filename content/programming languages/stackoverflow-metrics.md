+++
title = "Stackoverflow metrics"
description = "Top programming languages 2020 using Stackoverflow number of questions in tags"
categories = ['programming languages']
tags = ["stackoverflow"]
date = "2020-09-22"
draft = false
+++

[Stackoverflow](https://www.stackoverflow.com/) "is a question and answer site for professional and enthusiast programmers." [Wikipedia](https://en.wikipedia.org/wiki/Stack_Overflow)
This page shows metrics about subscrinumber of questions related to the programming languages tags

<!--more-->

## Questions

{{< preformatted_text file="/static/metrics_stackoverflow_tag_info.txt" >}}

Last update:
{{< text file="/static/metrics_stackoverflow_tag_info.txt.timestamp" >}}
