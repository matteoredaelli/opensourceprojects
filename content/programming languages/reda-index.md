+++
title = "Top programming Languages - REDA index"
description = "Top programming languages 2020 - REDA index"
categories = ['programming languages']
tags = ["programming languages"]
date = "2020-09-22"
draft = false
+++

Most of programming languages rankings like [TIOBE](https://www.tiobe.com/tiobe-index/) and [PYPL](http://pypl.github.io/PYPL.html) are built on search engines data (like Google trends). They are useful but too limited. See [Measuring_programming_language_popularity]( https://en.wikipedia.org/wiki/Measuring_programming_language_popularity)

REDA index aims to evaluate also
- Subreddits of [Reddit](https://reddit.com)
- Tags of [Stackoverflow](https://stackoverflow.com)
- Libraries and packages  [Libraries.io](https://libraries.io)
- Open job positions

At the moment REDA index is not yet available: I have started collecting some metrics: see you soon for updates ;-)
