+++
title = "Reddit metrics"
description = "Programming languages metrics from Reddit.com: active users and subscribers"
categories = ['programming languages']
tags = ["reddit"]
date = "2020-09-22"
draft = false
+++

[Reddit](https://www.reddit.com/) "is an American social news aggregation, web content rating, and discussion website" [Wikipedia](https://en.wikipedia.org/wiki/Reddit)
This page shows metrics about subscribers and online users of the Subreddits related to the programming languages.

<!--more-->

## Reddit subscribers

{{< preformatted_text file="/static/metrics_reddit_subscribers.txt" >}}

## Reddit active users

{{< preformatted_text file="/static/metrics_reddit_accounts_active.txt" >}}

Last update:
{{< text file="/static/metrics_reddit_subscribers.txt.timestamp" >}}
